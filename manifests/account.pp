# == Class: pivxd::account
#
# This class handles users/groups for the module
#
# == Actions:
#
# * Creates the pivx daemon's user and group
#
# === Authors:
#
# Craig Watson <craig@cwatson.org>
#
# === Copyright:
#
# Copyright (C) Craig Watson
# Published under the Apache License v2.0
#
class pivxd::account {

  user { $::pivxd::user_name:
    ensure     => present,
    comment    => 'pivx Daemon',
    home       => $::pivxd::user_home,
    managehome => true,
    shell      => '/bin/bash',
    gid        => $::pivxd::group_name,
    require    => Group[$::pivxd::group_name],
  }

  group { $::pivxd::group_name:
    ensure => present,
  }

}
