# == Class: pivxd
#
# Main class containing parameters and validation logic
#
# == Actions:
#
# * Fails on non-Ubuntu operating systems
# * Validates passed parameters
#
# === Authors:
#
# Craig Watson <craig@cwatson.org>
#
# === Copyright:
#
# Copyright (C) Craig Watson
# Published under the Apache License v2.0
#
class pivxd (
  Variant[Undef,Array]   $addnode                    = undef,
  Boolean                $allowreceivebyip           = true,
  Variant[Undef,String]  $alertnotify                = undef,
  String                 $pivxd_cmd               = '/usr/bin/pivxd',
  Variant[Undef,String]  $pivxd_datadir           = undef,
  Integer                $pivxd_nicelevel         = 0,
  Variant[Undef,String]  $pivxd_pidfile           = undef,
  Variant[Undef,String]  $blocknotify                = undef,
  Variant[Undef,Array]   $connect                    = undef,
  Boolean                $disablewallet              = false,
  Integer                $dbcache                    = 100,
  Boolean                $gen                        = false,
  String                 $group_name                 = 'pivxd',
  Boolean                $install_gui                = false,
  Integer                $keypool                    = 100,
  Integer                $limitfreerelay             = 15,
  Variant[Undef,String]  $minrelaytxfee              = undef,
  Integer                $maxconnections             = 125,
  Variant[Undef,String]  $maxuploadtarget            = undef,
  String                 $paytxfee                   = '0.00005',
  Boolean                $peerbloomfilters           = true,
  Variant[Undef,String]  $proxy                      = undef,
  Boolean                $server                     = true,
  Boolean                $testnet                    = false,
  Integer                $timeout                    = 5000,
  Boolean                $txindex                    = false,
  Variant[Undef,Array]   $rpcallowip                 = undef,
  Variant[Undef,String]  $rpcconnect                 = undef,
  String                 $rpcpassword                = 'EL5dW6NLpt3A8eeE2KBA9TcFyyVbNvhXfXNBpdB7Rcey',
  Integer                $rpcport                    = 8332,
  Boolean                $rpcssl                     = false,
  String                 $rpcsslcertificatechainfile = 'server.cert',
  String                 $rpcsslciphers              = 'TLSv1+HIGH:!SSLv2:!aNULL:!eNULL:!AH:!3DES:@STRENGTH',
  String                 $rpcsslprivatekeyfile       = 'server.pem',
  Integer                $rpctimeout                 = 30,
  String                 $rpcuser                    = 'pivxd',
  Boolean                $upnp                       = true,
  Boolean                $use_pivx_classic        = false,
  Variant[Undef,String]  $download_pivxd_version  = undef,
  String                 $download_pivxd_arch     = 'x86_64-linux-gnu',
  String                 $user_name                  = 'pivxd',
  String                 $user_home                  = '/home/pivxd',
  String                 $service_ensure             = 'running',
  Boolean                $service_enable             = true,
){

  # Hard-fail on anything that isn't Ubuntu
  if $::operatingsystem != 'Ubuntu' {
    fail('Unsupported operating system')
  }

  # Warn if install_gui and server are both true
  if $install_gui == true and $server == true {
    notify { 'pivxd warning':
      name     => 'install_gui and server are both set to true, server will be disabled!',
      withpath => true,
    }
  }

  if $connect != undef and $addnode != undef {
    fail('Can only use one of $connect and $allowip')
  }

  # Include all subclasses
  include ::pivxd::params
  include ::pivxd::account
  include ::pivxd::install
  include ::pivxd::config
  include ::pivxd::service

}
