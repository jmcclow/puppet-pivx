# == Class: pivxd::service
#
# Manages the pivxd service
#
# == Actions:
#
# None
#
# === Authors:
#
# Craig Watson <craig@cwatson.org>
#
# === Copyright:
#
# Copyright (C) Craig Watson
# Published under the Apache License v2.0
#
class pivxd::service {

  service { 'pivxd':
    ensure  => $::pivxd::service_ensure,
    enable  => $::pivxd::service_enable,
    require => File[$::pivxd::params::init_path,"${::pivxd::params::datadir}/pivx.conf"],
  }
}
