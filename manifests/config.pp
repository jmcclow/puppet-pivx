# == Class: pivxd::config
#
# This class handles the main configuration files for the module
#
# == Actions:
#
# * Creates the init script, data directory and pivxd configuration file
#
# === Authors:
#
# Craig Watson <craig@cwatson.org>
#
# === Copyright:
#
# Copyright (C) Craig Watson
# Published under the Apache License v2.0
#
class pivxd::config {

  file { $::pivxd::params::datadir:
    ensure  => directory,
    owner   => $::pivxd::user_name,
    group   => $::pivxd::group_name,
    mode    => '0755',
    require => User['pivxd'],
  }

  file { "${::pivxd::params::datadir}/pivx.conf":
    ensure  => file,
    owner   => $::pivxd::user_name,
    group   => $::pivxd::group_name,
    mode    => '0600',
    content => template('pivxd/pivx.conf.erb'),
    require => File[$::pivxd::params::datadir],
    notify  => Service['pivxd'],
  }

}
