# == Class: pivxd::install
#
# Installs packages
#
# == Actions:
#
# * Adds the pivx or pivx Classic Apt PPA to the system
# * Installs the pivxd package
# * Optionally installs the pivx-qt package
#
# === Authors:
#
# Craig Watson <craig@cwatson.org>
#
# === Copyright:
#
# Copyright (C) Craig Watson
# Published under the Apache License v2.0
#
class pivxd::install {


    ensure_packages(['wget','gzip'], { ensure => present })

    $filename = "pivx-${::pivxd::download_pivxd_version}-${::pivxd::download_pivxd_arch}.tar.gz"
    $url      = 'https://github.com/PIVX-Project/PIVX/releases/download/v2.2.1/pivx-2.2.1-x86_64-linux-gnu.tar.gz'

    exec { 'download_pivxd':
      command => "/usr/bin/wget -qO /tmp/${filename} ${url}",
      require => [Package['wget'],File[$::pivxd::params::datadir]],
      unless  => "/bin/ls -1 ${::pivxd::params::datadir} | grep -q download_install.done",
      notify  => Exec['uncompress_pivxd'],
    }

    exec { 'uncompress_pivxd':
      command     => "/bin/tar -xvfz /tmp/${filename}",
      cwd         => '/opt/',
      notify      => Exec['install_pivxd'],
      refreshonly => true,
    }

    file { 'link_pivxd':
      ensure      => 'link',
      target      => "/opt/pivx-${::pivxd::download_pivxd_version}"
    }

    exec { 'clean_downloaded_pivxd':
      command     => "/bin/rm -r /tmp/pivx-${::pivxd::download_pivxd_version}*",
      refreshonly => true,
    }


}
