# Class: pivxd::params
# Authors:
#
# Jay McClow <craig@cwatson.org>

class pivxd::params {

  if (versioncmp($::lsbdistrelease,'16.04') >= 0) {
    $init_path     = '/etc/systemd/system/pivxd.service'
    $init_template = 'systemd.erb'
  } else {
    $init_path     = '/etc/init/pivxd.conf'
    $init_template = 'upstart.erb'
  }

  if $::pivxd::download_pivxd_version != undef {
    $core_ppa_ensure    = absent
    $classic_ppa_ensure = absent
  } elsif $::pivxd::use_pivx_classic {
    $core_ppa_ensure    = absent
    $classic_ppa_ensure = present
  } else {
    $core_ppa_ensure    = present
    $classic_ppa_ensure = absent
  }

  if $::pivxd::pivxd_datadir != undef {
    $datadir = $::pivxd::pivxd_datadir
  } else {
    $datadir = "${pivxd::user_home}/.pivx"
  }

  if $::pivxd::pivxd_pidfile != undef {
    $pidfile = $::pivxd::pivxd_pidfile
  } else {
    $pidfile = "${::pivxd::params::datadir}/pivxd.pid"
  }

  if $::pivxd::install_gui == true {
    $server         = 0
    $service_ensure = stopped
    $service_enable = false
  } else {

    if $::pivxd::server == true {
      $server = 1
    } else {
      $server = 0
    }

    $service_ensure = running
    $service_enable = true
  }

  if $::pivxd::peerbloomfilters == true {
    $peerbloomfilters = 1
  } else {
    $peerbloomfilters = 0
  }

  if $::pivxd::disablewallet == true {
    $disablewallet = 1
  } else {
    $disablewallet = 0
  }

  if $::pivxd::txindex == true {
    $txindex = 1
  } else {
    $txindex = 0
  }

  if $::pivxd::upnp == true {
    $upnp = 1
  } else {
    $upnp = 0
  }

  if $::pivxd::testnet == true {
    $testnet = 1
  } else {
    $testnet = 0
  }

  if $::pivxd::rpcssl == true {
    $rpcssl = 1
  } else {
    $rpcssl = 0
  }

  if $::pivxd::gen == true {
    $gen = 1
  } else {
    $gen = 0
  }

  if $::pivxd::allowreceivebyip == true {
    $allowreceivebyip = 1
  } else {
    $allowreceivebyip = 0
  }

}
